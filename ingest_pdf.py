#!/usr/bin/env python
#
# Example usage:
# ./ingest_pdf.py --pdf-url=https://calhoun.nps.edu/bitstream/handle/10945/53069/Tracking_and_disrupting.pdf --host=nlp-demo.on.graphaspect.com:7474 --password=paper --only=enrich

import argparse
import os
import requests
import datetime
from py2neo import authenticate, Graph
from bs4 import BeautifulSoup
from bs4 import BeautifulSoup

parser = argparse.ArgumentParser(description='Load PDF into Neo4j, extract concepts, enrich')
parser.add_argument('--abstract-url', help='URL to PDF')
parser.add_argument('--pdf-url', help='URL to PDF')
parser.add_argument('--name', help='Document name')
parser.add_argument('--username', default='neo4j')
parser.add_argument('--password', default='paper')
#parser.add_argument('--password', default='neo4j')
parser.add_argument('--only')
parser.add_argument('--limit', type=int, default=9999999)
parser.add_argument('--batch-size', type=int, default=1)
parser.add_argument('--verbose', '-v', action='store_true')
parser.add_argument('--all', '-a', action='store_true')
parser.add_argument('--host', default='nlp-demo.on.graphaspect.com:7474', help='Neo4J Host')
#parser.add_argument('--host', default='127.0.0.1:7474', help='Neo4J Host')
args = parser.parse_args()

if args.abstract_url:
    soup = BeautifulSoup(requests.get(args.abstract_url).content, 'html.parser')
    args.name = soup.find_all('h2', 'page-header')[0].text
    args.pdf_url = soup.find_all('meta', attrs={'name':'citation_pdf_url'})[0].get('content')

if not args.name and args.pdf_url:
    args.name = os.path.split(args.pdf_url)[-1]

print '-' * 80
print 'Ingest begin %s' % (datetime.datetime.now())
print("Name     : %s" % (args.name))
print("URL      : %s" % (args.pdf_url))
print

authenticate(args.host, args.username, args.password)
graph = Graph('http://%s/db/data' % args.host)

if 'DELETE'==args.only:
    print 'deleting doc'
    cql = """MATCH (d:Document {name: '%s', url: '%s'})
    OPTIONAL MATCH
    (d)-[r1:HAS_PARAGRAPH]->(n1)
    OPTIONAL MATCH
    (n1)-[r2:HAS_ANNOTATED_TEXT]->(n2)
    OPTIONAL MATCH
    (n2)-[r3]->(n3:Sentence)
    OPTIONAL MATCH
    (n3)-[r4]-()
    DELETE d, r1, n2, r2, n2, r3, n3, r4
    """ % (args.name, args.pdf_url)
    if args.verbose: print(cql)
    graph.run(cql)

# Create document
if 'create'==args.only or not args.only:
    print 'create doc'
    cql = "MERGE (d:Document {name: '%s', url: '%s'})" % (args.name.replace("'", "\\'"), args.pdf_url)
    graph.run(cql)

# OCR
if 'ingest'==args.only or not args.only:
    print 'ingest'
    cql = """MATCH (d:Document) where d.url = '%s'
    CALL ga.nlp.parser.pdf(d.url)
    YIELD number, paragraphs
    UNWIND paragraphs as paragraph
    MERGE (d)-[:HAS_PARAGRAPH]->(p:Paragraph {number: number, text: paragraph})
    RETURN count(p) as num_paras
    """ % (args.pdf_url)
    if args.verbose: print cql
    res = graph.run(cql).next()
    from pprint import pprint
    pprint('Ingested paras: %s' % res[0])


# Clean (remove non-English, otherwise Annotate step crashes)
if 'clean'==args.only or not args.only:
    print 'clean'
    cql = """MATCH (d:Document {url: '%s'})-[r]->(p:Paragraph)
    CALL ga.nlp.detectLanguage(p.text)
    YIELD result
    WHERE result <> 'en'
    DELETE r, p""" % (args.pdf_url)
    if args.verbose: print(cql)
    graph.run(cql)

# Annotate
if 'annotate'==args.only or not args.only:
    print 'annotate'
    cql = """MATCH (d:Document)-[:HAS_PARAGRAPH]->(p)
        WHERE d.url='%s'
        AND NOT EXISTS((p)-[:HAS_ANNOTATED_TEXT]->(:AnnotatedText))
        RETURN count(p) as num_remaining
    """ % (args.pdf_url)
    print 'check progress with:'
    print cql
    print

    if args.all:
        cql = """CALL apoc.periodic.iterate(
                "MATCH (p:Paragraph)
                WHERE NOT (p)-[:HAS_ANNOTATED_TEXT]->(:AnnotatedText)
                RETURN p",
            """
    else:
        cql = """CALL apoc.periodic.iterate(
                "MATCH (d:Document)-[:HAS_PARAGRAPH]->(p)
                WHERE d.url='%s'
                AND NOT EXISTS((p)-[:HAS_ANNOTATED_TEXT]->(:AnnotatedText))
                RETURN p
		LIMIT %d",
            """ % (args.pdf_url, args.batch_size)

    cql += """
        "CALL ga.nlp.annotate({ text: p.text, id: id(p)})
        YIELD result
        MERGE (p)-[:HAS_ANNOTATED_TEXT]->(result)
        RETURN p"
        , {iterateList: true, batchSize: 1})
    """
    if args.verbose: print cql
    graph.run(cql)

# Enrich
if 'enrich'==args.only or not args.only:
    print 'enrich'

    if args.verbose: print('Check progress with:')
    cql = """MATCH (d:Document {url: '%s'})
    -[r]->(p:Paragraph)
    -[:HAS_ANNOTATED_TEXT]->(:AnnotatedText)
    --(:Sentence)
    --(t:Tag)
    WHERE not (t)-[:IS_RELATED_TO]->()
    RETURN count(t) as num_remaining""" % (args.pdf_url)
    if args.verbose: print(cql)


    # dodgy concats, beware ...
    if args.all:
        cql = """CALL apoc.periodic.iterate("MATCH"""
    else:
        cql = """CALL apoc.periodic.iterate(
        "MATCH (d:Document {url: '%s'})
        -[r]->(p:Paragraph)
        -[:HAS_ANNOTATED_TEXT]->(:AnnotatedText)
        --(:Sentence)
        --""" % (args.pdf_url)

    cql += """(t:Tag)
    WHERE NOT (t)-[:IS_RELATED_TO]->()
    RETURN t LIMIT 1",
    "CALL ga.nlp.enrich.concept({tag:t})"
    , {})"""
    if args.verbose: print cql
    graph.run(cql)


if 'report'==args.only or not args.only:
    cql = """MATCH (d:Document {url: '%s'})
    OPTIONAL MATCH
    (d)-[r]->(p:Paragraph)
    OPTIONAL MATCH
    (p)-[rAnnotated:HAS_ANNOTATED_TEXT]->(a:AnnotatedText)
    OPTIONAL MATCH
    (a)-[:CONTAINS_SENTENCE]->(:Sentence)-[:HAS_TAG]->(:Tag)-[rIsRelated:IS_RELATED_TO]->()
    RETURN  d.name as name,
            d.url as url,
            COUNT(DISTINCT p) as num_paragraphs,
            COUNT(DISTINCT rAnnotated) as num_annotated,
            COUNT(DISTINCT rIsRelated) as num_related
    """ % (args.pdf_url)
    res = graph.run(cql).next()
    for k in res.keys():
        print("%s %s" % (k.ljust(20), res[k]))

print 'Complete %s' % (datetime.datetime.now())
print '-' * 80
