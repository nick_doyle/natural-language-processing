//merge (d:Document {url:'https://www.nasa.gov/sites/default/files/atoms/files/iss_lessons_learned.pdf'})
merge (d:Document {url:'https://web.stanford.edu/class/polisci211z/1.1/Sun%20Tzu.pdf'})
with d
call ga.nlp.parser.pdf(d.url)
yield number, paragraphs
unwind paragraphs as text
with d,text
where length(text) > 5
merge (d)-[:HAS_PARAGRAPH]->(p:Paragraph {text: text})
return p,d


MATCH (p:Paragraph)
CALL ga.nlp.annotate({text: p.text, id: id(p), checkLanguage: False})
YIELD result
MERGE (p)-[:HAS_ANNOTATED_TEXT]->(result)
RETURN result


MATCH (n:Tag)
CALL ga.nlp.enrich.concept({enricher: 'conceptnet5', tag: n, depth:2, admittedRelationships:["IsA","PartOf"]})
YIELD result
RETURN result


MATCH (n:Tag {value:'materials'})
CALL ga.nlp.enrich.concept({enricher: 'conceptnet5', tag: n, depth:2, admittedRelationships:["IsA","PartOf"]})
YIELD result
RETURN result


match (n:NER_O)
call apoc.create.vNode({text: n.value})
yield result
return result
limit 25

