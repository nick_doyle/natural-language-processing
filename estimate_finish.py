#!/usr/bin/env python
#
# Example usage:
# ./ingest_pdf.py --pdf-url=https://calhoun.nps.edu/bitstream/handle/10945/53069/Tracking_and_disrupting.pdf --host=nlp-demo.on.graphaspect.com:7474 --password=paper --only=enrich

import argparse
import os
import requests
import time
import pytz
import datetime
from py2neo import authenticate, Graph
from bs4 import BeautifulSoup
from bs4 import BeautifulSoup

cql = """MATCH (p:Paragraph)
        WHERE NOT (p)-[:HAS_ANNOTATED_TEXT]->(:AnnotatedText)
        RETURN count(p) as num_unannotated
    """

SECS_TO_SLEEP = 5

authenticate('nlp-demo.on.graphaspect.com', 'neo4j', 'paper')
graph = Graph('http://nlp-demo.on.graphaspect.com:7474/db/data')

prev_time = time.time()
prev_num_annotated = graph.run(cql).next()[0]
avg_rate = 0

tz = pytz.timezone('Australia/Melbourne')

while True:
    time.sleep(SECS_TO_SLEEP)
    try:
        curr_num_unannotated = graph.run(cql).next()[0]
    except:
        print "can't connect ..."
        continue
    curr_time = time.time()
    curr_rate = (prev_num_annotated - curr_num_unannotated) / (curr_time - prev_time)

    if not avg_rate:
        avg_rate = curr_rate
    avg_rate = (avg_rate + curr_rate) / 2

    if avg_rate:
        seconds_to_complete = curr_num_unannotated / avg_rate
    else:
        seconds_to_complete = 99999999999

    print('curr_num_unannotated : %s') % (curr_num_unannotated)
    print('instant rate         : %s') % (curr_rate)
    print('avg rate             : %s') % (avg_rate)
    print('time to complete     : %s') % (datetime.timedelta(seconds=seconds_to_complete))
    print('eta                  : %s') % (datetime.datetime.fromtimestamp(curr_time + seconds_to_complete, tz=tz))
    print('_' * 80)
